//
//  EventDashboardViewController.swift
//  EventManager
//
//  Created by Steven on 06/01/2017.
//  Copyright © 2017 Asian Hospital and Medical Center. All rights reserved.
//

import UIKit
import AASquaresLoading
import Alamofire
import SCLAlertView

class EventDashboardViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    var validPincode: String!
    var event: Event!
    
    var participants: [Attendees]! {
        didSet {
            menuCollectionView.reloadData()
        }
    }
    var activeParticipants: [Attendees]! {
        didSet {
            menuCollectionView.reloadData()
        }
    }

    @IBOutlet weak var eventNameLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var datetimeLabel: UILabel!
    @IBOutlet weak var menuCollectionView: UICollectionView!
    
    fileprivate let menuItems = ["PARTICIPANTS", "REGISTERED", "QR CODE", "SEARCH NAME", "SYNC", "LOGOUT"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        menuCollectionView.dataSource = self
        menuCollectionView.delegate = self
        configureAALoading()
        
        guard event != nil,
              validPincode != nil else {
                
            _ = SCLAlertView(appearance: appearance).showError("No Event Details", subTitle: "There's no event details, please logout and try again")
            return
        }
        
        showEventDetails()
        getParticipants(enteredPincode: validPincode)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showParticipants" {
            if let participantsVC = segue.destination as? ParticipantViewController {
                participantsVC.validPincode = self.validPincode
                participantsVC.participants = self.participants
                participantsVC.titleString = "Participants"
            }
        }else if segue.identifier == "showActiveParticipants" {
            if let activeParticipantsVC = segue.destination as? ParticipantViewController {
                activeParticipantsVC.validPincode = self.validPincode
                activeParticipantsVC.participants = self.activeParticipants
                activeParticipantsVC.titleString = "Registered Participants"
            }
        }else if segue.identifier == "showQRCodeVC" {
            if let qrCodeVC = segue.destination as? QRCodeViewController {
                qrCodeVC.validPincode = self.validPincode
            }
        }else if segue.identifier == "showSearchNameVC" {
            if let showSearchNameVC = segue.destination as? SearchNameViewController {
                showSearchNameVC.validPincode = self.validPincode
                showSearchNameVC.participants = self.participants
            }
        }
    }
    
    // EVENTS: FUNCTIONS
    
    func configureAALoading() {
        self.view.squareLoading.color = UIColor(red: 80/255.0, green: 187/255.0, blue: 113/255.0, alpha: 1.0)
        self.view.squareLoading.backgroundColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.7)
    }
    
    func showEventDetails() {
        
        self.eventNameLabel.text = "\(event.name.uppercased())"
        self.locationLabel.text = "\(event.location.uppercased())"

        if let dateStringFromDate = getFormattedStringFromDate(date: event.startDateTime, formatString: "MMMM dd, yyyy/ hh:mm a") {
            self.datetimeLabel.text = "\(dateStringFromDate)"
        }else {
            self.datetimeLabel.text = "-"
        }
    }
    
    func getParticipants(enteredPincode: String) {
        
        //Show Loading
        self.view.squareLoading.start(0.0)
        
        // GET ALL PARTICIPANTS
        getAttendees(passcode: enteredPincode, participantType: .all) { (attendees, error) in
            
            guard let attendeesArray = attendees, error == nil else {
                
                if let networkError = error {
                    
                    // Hide loading view
                    self.view.squareLoading.stop(0.0)
                    _ = SCLAlertView(appearance: appearance).showError("Network Error", subTitle: "\(networkError.rawValue)")
                }
                
                return
            }
            
            self.participants = attendeesArray
            
            // GET ACTIVE PARTICIPANTS
            getAttendees(passcode: enteredPincode, participantType: .active) { (attendees, error) in
                
                guard let activeAttendeesArray = attendees, error == nil else {
                    
                    if let networkError = error {
                        
                        // Hide loading view
                        self.view.squareLoading.stop(0.0)
                        _ = SCLAlertView(appearance: appearance).showError("Network Error", subTitle: "\(networkError.rawValue)")
                    }
                    
                    return
                }
                
                self.activeParticipants = activeAttendeesArray
                self.view.squareLoading.stop(0.0)
            }
        }
    }
    
    func logout() {
        
        let alert = SCLAlertView(appearance: confirmationAppearance)
        _ = alert.addButton("Yes", action: {
            self.dismiss(animated: true, completion: nil)
        })
        _ = alert.addButton("No", action: { })
        _ = alert.showError("Confirmation", subTitle: "Are you sure you want to exit?")
        
    }
    
    //MARK: COLLECTION VIEW DATA SOURCE
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return menuItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! MenuCollectionViewCell
        let row = indexPath.row
        
        cell.backgroundCardView.layer.cornerRadius = 8.0
        cell.backgroundCardView.layer.shadowOffset = CGSize(width: 0, height: 0)
        cell.backgroundCardView.layer.shadowColor = UIColor.black.cgColor
        cell.backgroundCardView.layer.shadowRadius = 6
        cell.backgroundCardView.layer.shadowOpacity = 0.35
        cell.backgroundCardView.layer.masksToBounds = false
        cell.backgroundCardView.clipsToBounds = false
        
        cell.menuNameLabel.text = "\(menuItems[row])"
        
        switch(row) {
        case 0:
            cell.menuImageView.isHidden = true
            cell.countLabel.isHidden = false
            
            if participants != nil {
                cell.countLabel.text = "\(participants.count)"
            }else {
                cell.countLabel.text = "-"
            }
        case 1:
            cell.menuImageView.isHidden = true
            cell.countLabel.isHidden = false
            
            if activeParticipants != nil {
                cell.countLabel.text = "\(activeParticipants.count)"
            }else {
                cell.countLabel.text = "-"
            }
        case 2:
            cell.menuImageView.isHidden = false
            cell.countLabel.isHidden = true
            
            cell.menuImageView.image = UIImage(named: "qr-code")
        case 3:
            cell.menuImageView.isHidden = false
            cell.countLabel.isHidden = true
            
            cell.menuImageView.image = UIImage(named: "search")
        case 4:
            cell.menuImageView.isHidden = false
            cell.countLabel.isHidden = true
            
            cell.menuImageView.image = UIImage(named: "sync")
        case 5:
            cell.menuImageView.isHidden = false
            cell.countLabel.isHidden = true
            
            cell.menuImageView.image = UIImage(named: "logout")
        default: break
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let row = indexPath.row
        
        switch row {
        case 0: // PARTICIPANTS
            guard participants != nil else {
                _ = SCLAlertView(appearance: appearance).showWarning("No Participants", subTitle: "Please press the sync button to get the list of participants")
                return
            }
            
            if participants.count == 0 {
                _ = SCLAlertView(appearance: appearance).showWarning("No Participants", subTitle: "There's no participant at the moment")
            }else {
                self.performSegue(withIdentifier: "showParticipants", sender: self)
            }
        case 1: // ACTIVE PARTICIPANTS
            guard activeParticipants != nil else {
                _ = SCLAlertView(appearance: appearance).showWarning("No Participants", subTitle: "Please press the sync button to get the list of active participants")
                return
            }
            
            if activeParticipants.count == 0 {
                _ = SCLAlertView(appearance: appearance).showWarning("No Participants", subTitle: "There's no participant at the moment")
            }else {
                self.performSegue(withIdentifier: "showActiveParticipants", sender: self)
            }
        case 2: // QR CODE
            guard participants != nil else {
                _ = SCLAlertView(appearance: appearance).showWarning("No Participants", subTitle: "Please press the sync button to get the list of participants")
                return
            }
            
            if participants.count == 0 {
                _ = SCLAlertView(appearance: appearance).showWarning("No Participants", subTitle: "There's no participant at the moment")
            }else {
                self.performSegue(withIdentifier: "showQRCodeVC", sender: self)
            }
        case 3: // SEARCH NAME
            guard participants != nil else {
                _ = SCLAlertView(appearance: appearance).showWarning("No Participants", subTitle: "Please press the sync button to get the list of participants")
                return
            }
            
            if participants.count == 0 {
                _ = SCLAlertView(appearance: appearance).showWarning("No Participants", subTitle: "There's no participant at the moment")
            }else {
                self.performSegue(withIdentifier: "showSearchNameVC", sender: self)
            }
        case 4: // SYNC
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "noNeedToSync"), object: nil)
            getParticipants(enteredPincode: validPincode)
        case 5: // LOGOUT
            logout()
        default: break
        }
    }
    
}
