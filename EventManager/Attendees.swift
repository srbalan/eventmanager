//
//  Attendees.swift
//  EventManager
//
//  Created by Steven on 19/12/2016.
//  Copyright © 2016 Asian Hospital and Medical Center. All rights reserved.
//

import Foundation

struct Attendees {
    
    let id: String
    let employeeNumber: String?
    let lastName: String
    let firstName: String
    let middleName: String
    let displayName: String
    let department: String
    let position: String
    let registeredFlag: Bool
    let registeredDateTime: Date?
    let registrationType: Int
    let deleteFlag: Bool
    let manualFlag: Bool
    let outFlag: Bool
    let outDateTime: Date?
    let classification: Int?
    let others: String?
    
    init?(JSON: [String: AnyObject]) {
        
        guard let id = JSON["participant_id"] as? String else{
            return nil
        }
        
        self.id = id
        
        if let employeeNumber = JSON["employee_number"] as? String {
            self.employeeNumber = employeeNumber
        }else {
            self.employeeNumber = nil
        }
        
        if let lastName = JSON["last_name"] as? String {
            self.lastName = lastName.capitalized
        }else {
            self.lastName = ""
        }
        
        if let firstName = JSON["first_name"] as? String {
            self.firstName = firstName.capitalized
        }else {
            self.firstName = ""
        }
        
        if let middleName = JSON["middle_name"] as? String {
            self.middleName = middleName.capitalized
        }else {
            self.middleName = ""
        }
        
        if let displayName = JSON["display_name"] as? String {
            self.displayName = displayName.capitalized
        }else {
            self.displayName = ""
        }
        
        if let department = JSON["department_name"] as? String {
            self.department = department.capitalized
        }else {
            self.department = ""
        }
        
        if let position = JSON["position_name"] as? String {
            self.position = position.capitalized
        }else {
            self.position = ""
        }
        
        if let registeredFlag = JSON["registered_flag"] as? Bool {
            self.registeredFlag = registeredFlag
        }else {
            self.registeredFlag = false
        }
        
        if let registeredDateTime = JSON["registered_datetime"] as? String {
            if let formattedRegisteredDateTime = getDateFromString(dateString: registeredDateTime, formatString: "yyyy-MM-dd'T'HH:mm:ss.SSS") {
                self.registeredDateTime = formattedRegisteredDateTime
            }else {
                self.registeredDateTime = nil
            }
        }else {
            self.registeredDateTime = nil
        }
        
        // Registration Type:
        // 0 - Not yet registered
        // 1 - Search by name
        // 2 - QR Code
        
        if let registrationType = JSON["registration_type"] as? Int {
            self.registrationType = registrationType
        }else {
            self.registrationType = 0
        }
        
        if let deleteFlag = JSON["delete_flag"] as? Bool {
            self.deleteFlag = deleteFlag
        }else {
            self.deleteFlag = false
        }
        
        if let manualFlag = JSON["manual_reg_flag"] as? Bool {
            self.manualFlag = manualFlag
        }else {
            self.manualFlag = false
        }
        
        if let outFlag = JSON["out_flag"] as? Bool {
            self.outFlag = outFlag
        }else {
            self.outFlag = false
        }
        
        if let outDateTime = JSON["out_datetime"] as? String {
            if let formattedOutDateTime = getDateFromString(dateString: outDateTime, formatString: "yyyy-MM-dd'T'HH:mm:ss.SSS") {
                self.outDateTime = formattedOutDateTime
            }else {
                self.outDateTime = nil
            }
        }else {
            self.outDateTime = nil
        }
        
        if let classification = JSON["classification"] as? Int {
            self.classification = classification
        }else {
            self.classification = nil
        }
        
        if let others = JSON["others"] as? String {
            self.others = others
        }else {
            self.others = nil
        }

    }
}
