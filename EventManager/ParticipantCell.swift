//
//  ParticipantCell.swift
//  EventManager
//
//  Created by Application Developer on 15/02/2017.
//  Copyright © 2017 Asian Hospital and Medical Center. All rights reserved.
//

import UIKit
import FoldingCell
import SCLAlertView

class ParticipantCell: FoldingCell {

    @IBOutlet weak var displayNameLabel: UILabel!
    @IBOutlet weak var departmentLabel: UILabel!
    
    @IBOutlet weak var displayNameBigLabel: UILabel!
    @IBOutlet weak var employeeNumberLabel: UILabel!
    @IBOutlet weak var departmentDetailLabel: UILabel!
    @IBOutlet weak var positionLabel: UILabel!
    @IBOutlet weak var registeredDateTimeLabel: UILabel!
    @IBOutlet weak var registrationType: UILabel!
    @IBOutlet weak var outDateTimeLabel: UILabel!
    
    @IBOutlet weak var registrationStatusLabel: UILabel!
    @IBOutlet weak var registrationBackgroundSmallView: UIView!
    @IBOutlet weak var registrationBacgroundBigView: UIView!
    
    let notRegisteredColor = UIColor(red: 96/255.0, green: 125/255.0, blue: 139/255.0, alpha: 1.0)
    let registeredColor = UIColor(red: 121/255.0, green: 196/255.0, blue: 71/255.0, alpha: 1.0)
    let checkedOutColor = UIColor(red: 255/255.0, green: 84/255.0, blue: 84/255.0, alpha: 1.0)
    
    @IBOutlet weak var checkInOutButton: UIButton!
    
    var participant: Attendees! {
        didSet {
            setupParticipantDisplay()
        }
    }
    
    override func awakeFromNib() {
        
        foregroundView.layer.cornerRadius = 10
        foregroundView.layer.masksToBounds = true
        
        super.awakeFromNib()
    }
    
    override func animationDuration(_ itemIndex:NSInteger, type:AnimationType)-> TimeInterval {
        
        let durations = [0.26, 0.2, 0.2]
        return durations[itemIndex]
    }
    
    @IBAction func checkInOutButton(_ sender: UIButton) {
        
        guard let originalStatusLabel = checkInOutButton.titleLabel?.text! else {
            _ = SCLAlertView(appearance: appearance).showError("Error", subTitle: "There is something wrong with the participants. Please logout and try again")
            return
        }
        
        guard originalStatusLabel != "Loading..." else {
            return
        }
        
        var updateType: UpdateParticipantType!
        
        if originalStatusLabel == "CHECK IN" {
            updateType = .checkIn
        }else {
            updateType = .checkOut
        }
        
        //Show confirmation
        
        let alert = SCLAlertView(appearance: confirmationAppearance)
        _ = alert.addButton("Yes", action: {
            
            // Show Loading
            self.checkInOutButton.setTitle("Loading...", for: .normal)
            
            updateParticipant(updateType: updateType, participantID: self.participant.id, completion: { (success, error) in
                
                guard let success = success, error == nil else {
                    
                    if let networkError = error {
                        
                        switch(networkError) {
                        case .InvalidCredentials:
                            
                            if updateType == .checkOut {
                                _ = SCLAlertView(appearance: appearance).showError("Not Registered", subTitle: "Please register first before check out")
                            }
                        default:
                            _ = SCLAlertView(appearance: appearance).showError("Network Error", subTitle: "\(networkError.rawValue)")
                        }
                    }
                    
                    self.checkInOutButton.setTitle("\(originalStatusLabel)", for: .normal)
                    return
                }
                
                if success {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshParticipants"), object: nil)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "needToSync"), object: nil)
                }else {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshParticipants"), object: nil)
                }
            })
        })
        
        _ = alert.addButton("No", action: { })
        
        if updateType == .checkIn {
            _ = alert.showWarning("Check In", subTitle: "Are you sure you want to check in \(self.participant.displayName)?")
        }else if updateType == .checkOut {
            _ = alert.showWarning("Check Out", subTitle: "Are you sure you want to check out \(self.participant.displayName)?")
        }
    }
    
    //MARK: FUNCTIONS
    
    func setupParticipantDisplay() {
        if participant.registeredFlag == true {
            
            if participant.outFlag == true {
                setupStatusIndicator(smallView: registrationBackgroundSmallView, bigView: registrationBacgroundBigView, label: registrationStatusLabel, button: checkInOutButton, isRegistered: true, isCheckOut: true)
            }else {
                setupStatusIndicator(smallView: registrationBackgroundSmallView, bigView: registrationBacgroundBigView, label: registrationStatusLabel, button: checkInOutButton, isRegistered: true, isCheckOut: false)
            }
        }else {
            setupStatusIndicator(smallView: registrationBackgroundSmallView, bigView: registrationBacgroundBigView, label: registrationStatusLabel, button: checkInOutButton, isRegistered: false, isCheckOut: false)
        }
        
        self.displayNameLabel.text = participant.displayName
        self.displayNameBigLabel.text = participant.displayName
        self.departmentLabel.text = participant.department
        self.departmentDetailLabel.text = participant.department
        self.positionLabel.text = participant.position
        
        if participant.employeeNumber != nil {
            self.employeeNumberLabel.text = participant.employeeNumber
        }else {
            self.employeeNumberLabel.text = "-"
        }
        
        if let registeredDateTime = participant.registeredDateTime {
            if let formattedRegisteredDateTime = getFormattedStringFromDate(date: registeredDateTime, formatString: "MMMM dd, yyyy/ hh:mm a") {
                self.registeredDateTimeLabel.text = formattedRegisteredDateTime
            }else {
                self.registeredDateTimeLabel.text = "-"
            }
        }else {
            self.registeredDateTimeLabel.text = "-"
        }
        
        // Registration Type:
        // 0 - Not yet registered
        // 1 - Search by name
        // 2 - QR Code
        
        if participant.manualFlag {
            self.registrationType.text = "via Manual Registration"
        }else {
            switch participant.registrationType {
            case 0:
                self.registrationType.text = "-"
            case 1:
                self.registrationType.text = "via Name"
            case 2:
                self.registrationType.text = "via QR Code"
            default: break
            }
        }
        
        if let outDateTime = participant.outDateTime {
            if let formattedOutDateTime = getFormattedStringFromDate(date: outDateTime, formatString: "MMMM dd, yyyy/ hh:mm a") {
                self.outDateTimeLabel.text = formattedOutDateTime
            }else {
                self.outDateTimeLabel.text = "-"
            }
        }else {
            self.outDateTimeLabel.text = "-"
        }
    }
    
    func setupStatusIndicator(smallView: UIView, bigView: UIView, label: UILabel, button: UIButton, isRegistered: Bool, isCheckOut: Bool) {
        
        if (isRegistered == false && isCheckOut == false) {
            smallView.backgroundColor = notRegisteredColor
            bigView.backgroundColor = notRegisteredColor
            
            label.text = "NOT REGISTERED"
            
            button.isHidden = false
            button.backgroundColor = registeredColor
            button.setTitle("CHECK IN", for: .normal)
        }else if (isRegistered == true && isCheckOut == false) {
            smallView.backgroundColor = registeredColor
            bigView.backgroundColor = registeredColor
            
            label.text = "REGISTERED"
            
            button.isHidden = false
            button.backgroundColor = checkedOutColor
            button.setTitle("CHECK OUT", for: .normal)
        }else if (isRegistered == true && isCheckOut == true) {
            smallView.backgroundColor = checkedOutColor
            bigView.backgroundColor = checkedOutColor
            
            label.text = "CHECKED OUT"

            button.isHidden = true
        }
    }
    
}
