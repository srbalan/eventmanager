//
//  ManualRegistrationViewController.swift
//  EventManager
//
//  Created by Steven on 12/01/2017.
//  Copyright © 2017 Asian Hospital and Medical Center. All rights reserved.
//

import UIKit
import AASquaresLoading
import SCLAlertView

class ManualRegistrationViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    var validPincode: String!
    
    @IBOutlet weak var employeeNumberTextField: UITextField!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var middleNameTextField: UITextField!
    @IBOutlet weak var classificationTextField: UITextField!
    @IBOutlet weak var positionTextField: UITextField!
    @IBOutlet weak var departmentTextField: UITextField!
    @IBOutlet weak var personalBackgroundView: UIView!
    @IBOutlet weak var otherBackgroundView: UIView!
    
    let positionArray = ["","Application Developer", "Application Developer Manager", "CEO", "Director"]
    let departmentArray = ["","Medical Informatics", "Executive Office", "Medical Records"]
    let classificationArray = ["","Employee", "Outsource", "Consultant", "Others"]
    
    let classificationPickerView = UIPickerView()
    let positionPickerView = UIPickerView()
    let departmentPickerView = UIPickerView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupPickerView()
        configureAALoading()
        setupTextFieldBackground()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addButtonTapped(_ sender: UIButton) {
        
        guard var lastName = lastNameTextField.text,
            var firstName = firstNameTextField.text,
            var middleName = middleNameTextField.text,
            var employeeNumber = employeeNumberTextField.text,
            var classification = classificationTextField.text,
            var position = positionTextField.text,
            var department = departmentTextField.text
        else {
            
            return
        }
        
        lastName = removeWhitespace(rawString: lastName)
        firstName = removeWhitespace(rawString: firstName)
        middleName = removeWhitespace(rawString: middleName)
        employeeNumber = formatString(rawString: employeeNumber, acceptedCharacters: "0123456789-")
        classification = removeWhitespace(rawString: classification)
        position = removeWhitespace(rawString: position)
        department = removeWhitespace(rawString: department)
        
        guard lastName != "",
            firstName != "",
            classification != "" else{
            
            _ = SCLAlertView(appearance: appearance).showError("Incomplete", subTitle: "Please complete all required information")
                
            return
        }
        
        
        let alert = SCLAlertView(appearance: confirmationAppearance)
        _ = alert.addButton("Yes", action: {
            
            self.view.squareLoading.start(0.0)
            
            let participantDetails = AddAttendee(employeeNumber: employeeNumber, lastName: lastName.capitalized, firstName: firstName.capitalized, middleName: middleName.capitalized, departmentName: department.capitalized, positionName: position, classification: 0, others: classification.capitalized)
            
            addAttendee(passcode: self.validPincode, attendeeInfo: participantDetails) { (success, error) in
                
                guard let success = success, error == nil else {
                    
                    if let networkError = error {
                        
                        switch networkError {
                        case .ExistingRecord:
                            _ = SCLAlertView(appearance: appearance).showInfo("Existing", subTitle: "\(networkError.rawValue)")
                        case .InternalServerError:
                            _ = SCLAlertView(appearance: appearance).showError("Server Error", subTitle: "\(networkError.rawValue)")
                        default:
                            _ = SCLAlertView(appearance: appearance).showError("Network Error", subTitle: "\(networkError.rawValue)")
                        }
                        
                        // Hide loading view
                        self.view.squareLoading.stop(0.0)
                    }
                    
                    return
                }
                
                if success {
                    // Hide loading view
                    self.view.squareLoading.stop(0.0)
                    _ = SCLAlertView(appearance: appearance).showSuccess("Success", subTitle: "Welcome \(self.firstNameTextField.text!) \(self.lastNameTextField.text!)")
                    
                    self.clearTextFields()
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "needToSync"), object: nil)
                }
            }
        })
        _ = alert.addButton("No", action: { })
        
        _ = alert.showWarning("Register", subTitle: "Are you sure?")
    }
    
    // MARK: Functions
    
    func setupTextFieldBackground() {
        
        setupTextFieldBackgroundView(view: personalBackgroundView)
        setupTextFieldBackgroundView(view: otherBackgroundView)
    }
    
    func setupTextFieldBackgroundView(view: UIView) {
        
        view.layer.cornerRadius = 8.0
        view.layer.shadowOffset = CGSize(width: 0, height: 0)
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowRadius = 6
        view.layer.shadowOpacity = 0.35
        view.layer.masksToBounds = false
        view.clipsToBounds = false
    }
    
    func configureAALoading() {
        self.view.squareLoading.color = UIColor(red: 80/255.0, green: 187/255.0, blue: 113/255.0, alpha: 1.0)
        self.view.squareLoading.backgroundColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.7)
    }
    
    func setupPickerView() {
        
        classificationPickerView.delegate = self
        positionPickerView.delegate = self
        departmentPickerView.delegate = self
        
        classificationTextField.inputView = classificationPickerView
        classificationTextField.addTarget(self, action: #selector(ManualRegistrationViewController.displayInputViewForClassification), for: .touchUpInside)
        positionTextField.inputView = positionPickerView
        departmentTextField.inputView = departmentPickerView
        
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(ManualRegistrationViewController.donePicker))

        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.sizeToFit()
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolBar.setItems([flexibleSpace, doneButton], animated: false)
        
        classificationTextField.inputAccessoryView = toolBar
        positionTextField.inputAccessoryView = toolBar
        departmentTextField.inputAccessoryView = toolBar
    }
    
    func donePicker() {
        self.view.endEditing(true)
        setupPickerView()
    }
    
    func clearTextFields() {
        classificationTextField.text = ""
        employeeNumberTextField.text = ""
        firstNameTextField.text = ""
        lastNameTextField.text = ""
        middleNameTextField.text = ""
        positionTextField.text = ""
        departmentTextField.text = ""
    }
    
    func displayInputViewForClassification() {
        if self.classificationTextField.text == "" {
            classificationTextField.inputView = classificationPickerView
        }
    }
    
    // MARK: UIPickerView Delegate
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        switch pickerView {
        case classificationPickerView:
            return classificationArray.count
        case positionPickerView:
            return positionArray.count
        case departmentPickerView:
            return departmentArray.count
        default:
            return 1
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        switch pickerView {
        case classificationPickerView:
            return classificationArray[row]
        case positionPickerView:
            return positionArray[row]
        case departmentPickerView:
            return departmentArray[row]
        default:
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        switch pickerView {
        case classificationPickerView:
            if row == 4 {
                classificationTextField.text = ""
                classificationTextField.resignFirstResponder()
                classificationTextField.inputView = nil
                classificationTextField.becomeFirstResponder()
            }else {
                classificationTextField.text = classificationArray[row]
            }
        case positionPickerView:
            positionTextField.text = positionArray[row]
        case departmentPickerView:
            departmentTextField.text = departmentArray[row]
        default:
            break
        }
    }
    
    

}
