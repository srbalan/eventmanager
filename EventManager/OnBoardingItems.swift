//
//  OnBoardingItems.swift
//  EventManager
//
//  Created by Application Developer on 16/02/2017.
//  Copyright © 2017 Asian Hospital and Medical Center. All rights reserved.
//

import UIKit

let onBoardingItems: [(imageName: String, title: String, description: String, backgroundColor: UIColor)] = [
    
    (imageName: "web",
     title: "Web App",
     description: "Using your internet browser go to \nhttps://hopprlab.com/eventsmanager",
     backgroundColor: UIColor(red: 217/255.0, green: 72/255.0, blue: 89/255.0, alpha: 1.0)),
    
    (imageName: "credentials",
     title: "Login",
     description: "In the login page, enter your username and password",
     backgroundColor: UIColor(red: 106/255.0, green: 166/255.0, blue: 211/255.0, alpha: 1.0)),
    
    (imageName: "passcode",
     title: "Passcode",
     description: "After you successfully created an event, it will generate a passcode that you can use in the event manager mobile app",
     backgroundColor: UIColor(red: 21/255.0, green: 179/255.0, blue: 117/255.0, alpha: 1.0))
]
