//
//  MenuCollectionViewCell.swift
//  EventManager
//
//  Created by Application Developer on 25/01/2017.
//  Copyright © 2017 Asian Hospital and Medical Center. All rights reserved.
//

import UIKit

class MenuCollectionViewCell: UICollectionViewCell {
    
    var notificationCount = 0
    
    @IBOutlet weak var menuImageView: UIImageView!
    @IBOutlet weak var menuNameLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var backgroundCardView: UIView!
    @IBOutlet weak var syncNotificationCount: UILabel!
    
    override func awakeFromNib() {
        configureSyncNotificationObserver()
    }
    
    //MARK: FUNCTIONS
    func configureSyncNotificationObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.addNotificationCount), name: NSNotification.Name(rawValue: "needToSync"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.dontShowSyncNotification), name: NSNotification.Name(rawValue: "noNeedToSync"), object: nil)
    }
    
    func dontShowSyncNotification() {
        if menuNameLabel.text == "SYNC" {
            notificationCount = 0
            syncNotificationCount.isHidden = true
        }
    }
    
    func addNotificationCount() {
        if menuNameLabel.text == "SYNC" {
            notificationCount = notificationCount + 1
            syncNotificationCount.text = "\(notificationCount)"
            syncNotificationCount.isHidden = false
        }
    }
}
