//
//  ParticipantsTableViewCell.swift
//  EventManager
//
//  Created by Application Developer on 17/01/2017.
//  Copyright © 2017 Asian Hospital and Medical Center. All rights reserved.
//

import UIKit

class ParticipantsTableViewCell: UITableViewCell {

    @IBOutlet weak var participantNameLabel: UILabel!
    @IBOutlet weak var participantDepartmentLabel: UILabel!
    @IBOutlet weak var statusBackground: UIView!
    @IBOutlet weak var statusLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
