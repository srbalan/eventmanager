//
//  PasscodeViewController.swift
//  EventManager
//
//  Created by Application Developer on 01/02/2017.
//  Copyright © 2017 Asian Hospital and Medical Center. All rights reserved.
//

import UIKit
import AASquaresLoading
import SCLAlertView

class PasscodeViewController: UIViewController {

    @IBOutlet weak var passcodeOneLabel: UILabel!
    @IBOutlet weak var passcodeTwoLabel: UILabel!
    @IBOutlet weak var passcodeThreeLabel: UILabel!
    @IBOutlet weak var passcodeFourLabel: UILabel!
    
    fileprivate var enteredPasscode = ""
    fileprivate var event: Event!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureAALoading()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showEventDashboard" {
            if let eventDashboardVC = segue.destination as? EventDashboardViewController {
                eventDashboardVC.event = self.event
                eventDashboardVC.validPincode = self.enteredPasscode
            }
        }
    }
    
    @IBAction func numericButtonTapped(_ sender: UIButton) {
        
        enteredPasscode = enteredPasscode + sender.currentTitle!
        
        if passcodeOneLabel.isHidden {
            passcodeOneLabel.isHidden = false
        }else if passcodeTwoLabel.isHidden {
            passcodeTwoLabel.isHidden = false
        }else if passcodeThreeLabel.isHidden {
            passcodeThreeLabel.isHidden = false
        }else if passcodeFourLabel.isHidden {
            passcodeFourLabel.isHidden = false
            
            validateEventPasscode()
        }
    }
    
    @IBAction func deleteButtonTapped(_ sender: UIButton) {
        
        guard passcodeOneLabel.isHidden == false,
            enteredPasscode != "" else {
                return
        }
        
        enteredPasscode.remove(at: enteredPasscode.index(before: enteredPasscode.endIndex))
        
        if passcodeThreeLabel.isHidden == false {
            passcodeThreeLabel.isHidden = true
        }else if passcodeTwoLabel.isHidden == false {
            passcodeTwoLabel.isHidden = true
        }else if passcodeOneLabel.isHidden == false {
            passcodeOneLabel.isHidden = true
        }
    }
    
    @IBAction func canceButtonTapped(_ sender: UIButton) {
        clearPasscode()
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: FUNCTIONS
    
    func configureAALoading() {
        self.view.squareLoading.color = UIColor(red: 80/255.0, green: 187/255.0, blue: 113/255.0, alpha: 1.0)
        self.view.squareLoading.backgroundColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.7)
    }
    
    func clearPasscode() {
        
        enteredPasscode = ""
        
        passcodeOneLabel.isHidden = true
        passcodeTwoLabel.isHidden = true
        passcodeThreeLabel.isHidden = true
        passcodeFourLabel.isHidden = true
    }
    
    func validateEventPasscode() {
        // Show Loading
        self.view.squareLoading.start(0.0)
        
        validatePasscode(passcode: enteredPasscode) { (event, error) in
            
            guard let eventDetails = event, error == nil else {
                
                if let networkError = error {
                    
                    if networkError == .InvalidCredentials {
                        _ = SCLAlertView(appearance: appearance).showError("Ooops!", subTitle: "Please enter a valid event passcode")
                    }else {
                        _ = SCLAlertView(appearance: appearance).showError("Network Error", subTitle: "\(networkError.rawValue)")
                    }
                }
                
                self.view.squareLoading.stop(0.0)
                self.clearPasscode()
                
                return
            }
            
            guard eventDetails.deleteFlag == false else {
                
                _ = SCLAlertView(appearance: appearance).showError("Ooops!", subTitle: "Please enter a valid event passcode")
                
                self.view.squareLoading.stop(0.0)
                self.clearPasscode()
                
                return
            }
            
            if (eventDetails.closedFlag == true && eventDetails.reopenFlag == false) {
                _ = SCLAlertView(appearance: appearance).showError("Closed Event", subTitle: "Please check the status of your event and try again")
                
                self.view.squareLoading.stop(0.0)
                return
            }
            
            // VALID PASSCODE AND NOT DELETED OR CLOSED EVENT
            
            self.event = eventDetails
            self.view.squareLoading.stop(0.0)
            
            self.performSegue(withIdentifier: "showEventDashboard", sender: self)
            self.clearPasscode()
        }
    }
}
