//
//  PublicFunctions.swift
//  EventManager
//
//  Created by HopprLab on 18/05/2017.
//  Copyright © 2017 Asian Hospital and Medical Center. All rights reserved.
//

import Foundation

func getDateFromString(dateString: String, formatString: String) -> Date? {
    
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = formatString
    dateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00")
    if let date = dateFormatter.date(from: dateString) {
        return date
    }else {
        return nil
    }
}

func getFormattedStringFromDate(date: Date, formatString: String) -> String? {
    
    let formatter = DateFormatter()
    formatter.timeZone = TimeZone(abbreviation: "GMT+0:00")
    formatter.dateFormat = formatString
    let dateStringFromDate = formatter.string(from: date)
    
    return dateStringFromDate
}

func formatString(rawString: String, acceptedCharacters: String) -> String {
    
    let formattedString = String(rawString.characters.filter {
        String($0).rangeOfCharacter(from: CharacterSet(charactersIn: acceptedCharacters)) != nil
    })
    
    return formattedString
}

func removeWhitespace(rawString: String) -> String {
    
    let formattedString = rawString.trimmingCharacters(in: .whitespaces)
    
    return formattedString
}

