//
//  DialogBox.swift
//  EventManager
//
//  Created by Steven on 06/01/2017.
//  Copyright © 2017 Asian Hospital and Medical Center. All rights reserved.
//

import SCLAlertView

let appearance = SCLAlertView.SCLAppearance(kTitleHeight: 50.0, kWindowWidth: 400.0, kWindowHeight: 400.0, kTextHeight: 150.0, kTextViewdHeight: 400.0, kButtonHeight: 65.0, kTitleFont: UIFont(name: "HelveticaNeue", size: 25)!, kTextFont: UIFont(name: "HelveticaNeue", size: 18)!, kButtonFont: UIFont(name: "HelveticaNeue", size: 20)!, hideWhenBackgroundViewIsTapped: true)

let confirmationAppearance = SCLAlertView.SCLAppearance(kTitleHeight: 50.0, kWindowWidth: 400.0, kWindowHeight: 400.0, kTextHeight: 150.0, kTextViewdHeight: 400.0, kButtonHeight: 65.0, kTitleFont: UIFont(name: "HelveticaNeue", size: 25)!, kTextFont: UIFont(name: "HelveticaNeue", size: 18)!, kButtonFont: UIFont(name: "HelveticaNeue", size: 20)!, showCloseButton: false , hideWhenBackgroundViewIsTapped: false)
