//
//  EventMenuViewController.swift
//  EventManager
//
//  Created by Steven on 19/12/2016.
//  Copyright © 2016 Asian Hospital and Medical Center. All rights reserved.
//

import UIKit
import SearchTextField
import SCLAlertView
import AVFoundation
import QRCodeReader
import AASquaresLoading
import RAMPaperSwitch
import SAConfettiView

class EventMenuViewController: UIViewController, QRCodeReaderViewControllerDelegate {
    
    var validPincode: String!
    var eventName: String!
    var participants: [Attendees]!
    var activeParticipants: [Attendees]!
    var confettiView: SAConfettiView!

    @IBOutlet weak var searchBackgroundView: UIView!
    @IBOutlet weak var searchAttendeesTextField: SearchTextField!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var lockButton: UIButton!
    @IBOutlet weak var checkInOutLabel: UILabel!
    @IBOutlet weak var checkInOutSwitch: RAMPaperSwitch!
    @IBOutlet weak var qrcodeView: UIView!
    @IBOutlet weak var manualView: UIView!
    @IBOutlet weak var eventNameLabel: UILabel!
    
    lazy var readerVC: QRCodeReaderViewController = {
        let builder = QRCodeReaderViewControllerBuilder {
            $0.reader = QRCodeReader(metadataObjectTypes: [AVMetadataObjectTypeQRCode], captureDevicePosition: .back)
        }
        return QRCodeReaderViewController(builder: builder)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(EventMenuViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)

        setupSearchTextField()
        configureAALoading()
        setupView(view: qrcodeView)
        setupView(view: manualView)
        
        checkInOutSwitch.addTarget(self, action: #selector(switchValueDidChange), for: .valueChanged)
        
        eventNameLabel.text = eventName
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showAddParticipant" {
            if let manualRegistrationVC = segue.destination as? ManualRegistrationViewController {
                manualRegistrationVC.validPincode = validPincode
            }
        }
    }
    
    @IBAction func addParticipantButtonTapped(_ sender: UIButton) {
        self.performSegue(withIdentifier: "showAddParticipant", sender: self)
    }
    
    @IBAction func scanButtonTapped(_ sender: UIButton) {
        
        readerVC.delegate = self
        
        readerVC.completionBlock = { (result: QRCodeReaderResult?) in
            // Show Loading
            self.view.squareLoading.start(0.0)
        }
        
        readerVC.modalPresentationStyle = .fullScreen
        present(readerVC, animated: true, completion: nil)
    }
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func lockButtonTapped(_ sender: UIButton) {
        
        let buttonImage = sender.currentImage

        if buttonImage == UIImage(named: "locked") {
            showUnlockView()
        }else if buttonImage == UIImage(named: "unlocked") {
            lockButton.setImage(UIImage(named: "locked"), for: .normal)
            backButton.isHidden = true
        }
    }
    
    // MARK: FUNCTIONS

    func showConfetti() {
        
        confettiView = SAConfettiView(frame: self.view.bounds)
        confettiView.intensity = 0.5
        confettiView.type = .Confetti
        view.addSubview(confettiView)
        
        confettiView.startConfetti()
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2.0, execute: {
            self.confettiView.stopConfetti()
        })
    }
    
    func showUnlockView() {
        let alert = SCLAlertView(appearance: confirmationAppearance)
        let passcode = alert.addTextField("Passcode")
        passcode.isSecureTextEntry = true
        passcode.keyboardType = .decimalPad
        alert.addButton("Unlock") {
            if passcode.text == self.validPincode {
                self.lockButton.setImage(UIImage(named: "unlocked"), for: .normal)
                self.backButton.isHidden = false
                _ = SCLAlertView(appearance: appearance).showSuccess("Unlocked", subTitle: "")
            }else {
                _ = SCLAlertView(appearance: appearance).showError("Ooops!", subTitle: "Please enter a valid event passcode to unlock")
            }
        }
        alert.addButton("Cancel", action: { })
        alert.showNotice("Unlock", subTitle: "Please enter event passcode to unlock")
        
        // To focus in text field and show the keyboard
        passcode.becomeFirstResponder()
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func setupView(view: UIView) {
        
        view.layer.cornerRadius = 8.0
        view.layer.shadowOffset = CGSize(width: 0, height: 0)
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowRadius = 6
        view.layer.shadowOpacity = 0.35
        view.layer.masksToBounds = false
        view.clipsToBounds = false
    }
    
    func updateActiveParticipants() {
        
        // Show Loading
        self.view.squareLoading.start(0.0)
        
        getAttendees(passcode: validPincode, participantType: .active) { (attendees, error) in
            
            guard let activeAttendeesArray = attendees, error == nil else {
                
                if let networkError = error {
                    
                    // Hide loading view
                    self.view.squareLoading.stop(0.0)
                    _ = SCLAlertView(appearance: appearance).showError("Network Error", subTitle: "\(networkError.rawValue)")
                }
                
                return
            }
            
            if activeAttendeesArray.count == 0 {
                _ = SCLAlertView(appearance: appearance).showError("No Registered Participant", subTitle: "No registered participant to check out at the moment")
                self.checkInOutSwitch.isOn = false
            }else {
                self.activeParticipants = activeAttendeesArray
                self.setupSearchTextField()
            }
            
            // Hide loading view
            self.view.squareLoading.stop(0.0)
        }
    }
    
    func switchValueDidChange() {
        
        if checkInOutSwitch.isOn {
            checkInOutLabel.text = "CHECK OUT"
            updateActiveParticipants()
        }else {
            setupSearchTextField()
            checkInOutLabel.text = "CHECK IN"
        }
        
        searchAttendeesTextField.text = ""
    }
    
    func configureAALoading() {
        self.view.squareLoading.color = UIColor(red: 80/255.0, green: 187/255.0, blue: 113/255.0, alpha: 1.0)
        self.view.squareLoading.backgroundColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.7)
    }
    
    func setupSearchTextField() {
        
        var searchTextFieldItems: [SearchTextFieldItem] = []
        
        var suggestionParticipant: [Attendees]!
        
        if self.checkInOutSwitch.isOn {
            suggestionParticipant = self.activeParticipants
        }else {
            suggestionParticipant = self.participants
        }
        
        guard suggestionParticipant.count > 0 else {
            _ = SCLAlertView(appearance: appearance).showWarning("No Participants", subTitle: "Please add your participants")
            return
        }
        
        for index in 0...(suggestionParticipant.count - 1) {
            
            let id = "\(suggestionParticipant[index].id)"
            let displayName = "\(suggestionParticipant[index].displayName)"
            let department = "\(suggestionParticipant[index].department)"
            
            searchTextFieldItems.append(SearchTextFieldItem(title: displayName, subtitle: department, id: id))
        }
        
        self.searchAttendeesTextField.comparisonOptions = [.caseInsensitive]
        
        self.searchAttendeesTextField.filterItems(searchTextFieldItems)
        
        searchAttendeesTextField.theme.font = UIFont.systemFont(ofSize: 25.0)
        searchAttendeesTextField.theme.cellHeight = 75
        searchAttendeesTextField.theme.bgColor = UIColor.white
        
        searchBackgroundView.layer.cornerRadius = 5.0
        searchBackgroundView.layer.masksToBounds = false
        searchBackgroundView.layer.shadowOffset = CGSize(width: 0, height: 0)
        searchBackgroundView.layer.shadowColor = UIColor.black.cgColor
        searchBackgroundView.layer.shadowRadius = 3.0
        searchBackgroundView.layer.shadowOpacity = 0.75
        searchBackgroundView.clipsToBounds = false

        searchAttendeesTextField.maxResultsListHeight = 420
        
        searchAttendeesTextField.highlightAttributes = [NSBackgroundColorAttributeName: UIColor.white , NSFontAttributeName: UIFont.boldSystemFont(ofSize: 25.0)]
        
        // SELECTION FROM SEARCH TEXT FIELD
        
        searchAttendeesTextField.itemSelectionHandler = { item, itemPosition in
            
            self.view.endEditing(true)
            
            if let id = item.id {
                
                var updateType: UpdateParticipantType!
                
                if self.checkInOutSwitch.isOn {
                    updateType = .checkOut
                }else {
                    updateType = .checkIn
                }
                
                //Show confirmation
                
                let alert = SCLAlertView(appearance: confirmationAppearance)
                _ = alert.addButton("Yes", action: {
                
                    // Show Loading
                    self.view.squareLoading.start(0.0)
                    
                    self.searchAttendeesTextField.text = item.title
                    
                    updateParticipant(updateType: updateType, participantID: id, completion: { (success, error) in
                        
                        guard let success = success, error == nil else {
                            
                            if let networkError = error {
                                
                                switch(networkError) {
                                case .InvalidCredentials:
                                    
                                    if updateType == .checkOut {
                                        _ = SCLAlertView(appearance: appearance).showError("Not Registered", subTitle: "Please register first before check out")
                                    }
                                default:
                                    _ = SCLAlertView(appearance: appearance).showError("Network Error", subTitle: "\(networkError.rawValue)")
                                }
                                
                                self.searchAttendeesTextField.text = ""
                                
                                // Hide loading view
                                self.view.squareLoading.stop(0.0)
                            }
                            return
                        }
                        
                        if success {
                            self.showConfetti()
                            _ = SCLAlertView(appearance: appearance).showSuccess("Success", subTitle: "Thank You \(item.title)").setDismissBlock {
                                self.confettiView.removeFromSuperview()
                            }
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "needToSync"), object: nil)
                        }else {
                            _ = SCLAlertView(appearance: appearance).showInfo("Registered", subTitle: "Hi \(item.title), you are already registered")
                        }
                        
                        self.searchAttendeesTextField.text = ""
                        self.view.squareLoading.stop(0.0)
                    })
                    
                })
                
                _ = alert.addButton("No", action: { })
                
                if updateType == .checkIn {
                    _ = alert.showWarning("Check In", subTitle: "Are you \(item.title)?")
                }else if updateType == .checkOut {
                    _ = alert.showWarning("Check Out", subTitle: "Are you \(item.title)?")
                }
            }
        }
    }
    
    func validateEmployeeNumber(employeeNumber: String) {
        
        var updateType: UpdateParticipantType!
        
        if self.checkInOutSwitch.isOn {
            updateType = .checkOut
        }else {
            updateType = .checkIn
        }
        
        updateParticipantQR(updateType: updateType, passcode: validPincode, employeeNumber: employeeNumber, completion: { (participant, error) in
            
            guard let participantDetails = participant, error == nil else {
                
                if let networkError = error {
                    
                    switch(networkError) {
                    case .AlreadyRegistered:
                        _ = SCLAlertView(appearance: appearance).showInfo("Registered", subTitle: "\(networkError.rawValue)")
                    case .NotYetRegistered:
                        _ = SCLAlertView(appearance: appearance).showError("Error", subTitle: "\(networkError.rawValue)")
                    case .InvalidCredentials:
                        _ = SCLAlertView(appearance: appearance).showError("Invalid", subTitle: "QR Code is invalid. Please try again")
                    default:
                        _ = SCLAlertView(appearance: appearance).showError("Network Error", subTitle: "\(networkError.rawValue)")
                    }
                }
                
                // Hide loading view
                self.view.squareLoading.stop(0.0)
                
                return
            }

            self.showConfetti()
            _ = SCLAlertView(appearance: appearance).showSuccess("Success", subTitle: "Thank You \(participantDetails.lastName), \(participantDetails.firstName)").setDismissBlock {
                self.confettiView.removeFromSuperview()
            }

            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "needToSync"), object: nil)
            
            // Hide loading view
            self.view.squareLoading.stop(0.0)
        })
    }

    // MARK: QRCodeReaderViewController Delegate Methods
    
    func reader(_ reader: QRCodeReaderViewController, didScanResult result: QRCodeReaderResult) {
        
        reader.stopScanning()
        dismiss(animated: true, completion: {
            self.validateEmployeeNumber(employeeNumber: result.value)
        })
        
    }
    
    func reader(_ reader: QRCodeReaderViewController, didSwitchCamera newCaptureDevice: AVCaptureDeviceInput) {
        if let cameraName = newCaptureDevice.device.localizedName {
            //print("Switching capture to: \(cameraName)")
        }
    }
    
    func readerDidCancel(_ reader: QRCodeReaderViewController) {
        reader.stopScanning()
        dismiss(animated: true, completion: nil)
        
        // Hide loading view
        self.view.squareLoading.stop(0.0)
    }

}
