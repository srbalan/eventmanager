//
//  AddAttendee.swift
//  EventManager
//
//  Created by Application Developer on 18/01/2017.
//  Copyright © 2017 Asian Hospital and Medical Center. All rights reserved.
//

import Foundation

struct AddAttendee {
    
    let employeeNumber: String
    let lastName: String
    let firstName: String
    let middleName: String
    let departmentName: String
    let positionName: String
    let classification: Int
    let others: String
}
