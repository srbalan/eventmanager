//
//  SearchNameViewController.swift
//  EventManager
//
//  Created by HopprLab on 18/05/2017.
//  Copyright © 2017 Asian Hospital and Medical Center. All rights reserved.
//

import UIKit
import SearchTextField
import SCLAlertView
import AASquaresLoading
import SAConfettiView

private let bannerImageName = "AHMC"

class SearchNameViewController: UIViewController {
    
    var validPincode: String!
    var participants: [Attendees]!
    var confettiView: SAConfettiView!
    
    @IBOutlet weak var searchBackgroundView: UIView!
    @IBOutlet weak var searchAttendeesTextField: SearchTextField!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var lockButton: UIButton!
    @IBOutlet weak var checkInOutLabel: UILabel!
    @IBOutlet weak var checkInOutSwitch: UISwitch!
    @IBOutlet weak var bannerImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupSearchTextField()
        configureAALoading()
        self.bannerImageView.image = UIImage(named: bannerImageName)
        self.checkInOutSwitch.isOn = false
        setupView(view: searchBackgroundView)
        self.checkInOutSwitch.addTarget(self, action: #selector(switchValueDidChange), for: .valueChanged)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.searchAttendeesTextField.becomeFirstResponder()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        view.endEditing(true)
    }

    // MARK: ACTIONS
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func lockButtonTapped(_ sender: UIButton) {
        
        let buttonImage = sender.currentImage
        
        if buttonImage == UIImage(named: "locked") {
            showUnlockView()
        }else if buttonImage == UIImage(named: "unlocked") {
            lockButton.setImage(UIImage(named: "locked"), for: .normal)
            backButton.isHidden = true
        }
    }
    
    // MARK: FUNCTIONS
    
    
    
    private func configureAALoading() {
        self.view.squareLoading.color = UIColor(red: 80/255.0, green: 187/255.0, blue: 113/255.0, alpha: 1.0)
        self.view.squareLoading.backgroundColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.7)
    }
    
    private func setupView(view: UIView) {
        
        view.layer.cornerRadius = 8.0
        view.layer.shadowOffset = CGSize(width: 0, height: 0)
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowRadius = 6
        view.layer.shadowOpacity = 0.35
        view.layer.masksToBounds = false
        view.clipsToBounds = false
    }
    
    private func showUnlockView() {
        let alert = SCLAlertView(appearance: confirmationAppearance)
        let passcode = alert.addTextField("Passcode")
        passcode.isSecureTextEntry = true
        passcode.keyboardType = .decimalPad
        alert.addButton("Unlock") {
            if passcode.text == self.validPincode {
                self.lockButton.setImage(UIImage(named: "unlocked"), for: .normal)
                self.backButton.isHidden = false
                _ = SCLAlertView(appearance: appearance).showSuccess("Unlocked", subTitle: "")
            }else {
                _ = SCLAlertView(appearance: appearance).showError("Ooops!", subTitle: "Please enter a valid event passcode to unlock")
            }
        }
        alert.addButton("Cancel", action: { })
        alert.showNotice("Unlock", subTitle: "Please enter event passcode to unlock")
        
        // To focus in text field and show the keyboard
        passcode.becomeFirstResponder()
    }
    
    private func showConfetti() {
        
        confettiView = SAConfettiView(frame: self.view.bounds)
        confettiView.intensity = 0.5
        confettiView.type = .Confetti
        view.addSubview(confettiView)
        
        confettiView.startConfetti()
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2.0, execute: {
            self.confettiView.stopConfetti()
        })
    }
    
    func switchValueDidChange() {
        var title = ""
        
        if checkInOutSwitch.isOn {
            title = "CHECK OUT"
        }else {
            title = "CHECK IN"
        }
        
        if let checkLabel = checkInOutLabel {
            checkLabel.pushTransition(0.4)
            checkLabel.text = title
        }
        
        self.searchAttendeesTextField.becomeFirstResponder()
    }
    
    private func setupSearchTextField() {
        
        var searchTextFieldItems: [SearchTextFieldItem] = []
        
        guard self.participants.count > 0 else {
            _ = SCLAlertView(appearance: appearance).showWarning("No Participants", subTitle: "Please add your participants")
            return
        }
        
        for index in 0...(self.participants.count - 1) {
            
            let id = "\(self.participants[index].id)"
            let displayName = "\(self.participants[index].displayName)"
            let department = "\(self.participants[index].department)"
            
            searchTextFieldItems.append(SearchTextFieldItem(title: displayName, subtitle: department, id: id))
        }
        
        self.searchAttendeesTextField.comparisonOptions = [.caseInsensitive]
        
        self.searchAttendeesTextField.filterItems(searchTextFieldItems)
        
        searchAttendeesTextField.theme.font = UIFont.systemFont(ofSize: 25.0)
        searchAttendeesTextField.theme.cellHeight = 75
        searchAttendeesTextField.theme.bgColor = UIColor.white
        
        searchBackgroundView.layer.cornerRadius = 5.0
        searchBackgroundView.layer.masksToBounds = false
        searchBackgroundView.layer.shadowOffset = CGSize(width: 0, height: 0)
        searchBackgroundView.layer.shadowColor = UIColor.black.cgColor
        searchBackgroundView.layer.shadowRadius = 3.0
        searchBackgroundView.layer.shadowOpacity = 0.75
        searchBackgroundView.clipsToBounds = false
        
        searchAttendeesTextField.maxResultsListHeight = 420
        
        searchAttendeesTextField.highlightAttributes = [NSBackgroundColorAttributeName: UIColor.white , NSFontAttributeName: UIFont.boldSystemFont(ofSize: 25.0)]
        
        // SELECTION FROM SEARCH TEXT FIELD
        
        searchAttendeesTextField.itemSelectionHandler = { item, itemPosition in
            
            self.view.endEditing(true)
            
            if let id = item.id {
                
                var updateType: UpdateParticipantType!
                
                if self.checkInOutSwitch.isOn {
                    updateType = .checkOut
                }else {
                    updateType = .checkIn
                }
                
                //Show confirmation
                
                let alert = SCLAlertView(appearance: confirmationAppearance)
                _ = alert.addButton("Yes", action: {
                    
                    // Show Loading
                    self.view.squareLoading.start(0.0)
                    
                    self.searchAttendeesTextField.text = item.title
                    
                    updateParticipant(updateType: updateType, participantID: id, completion: { (success, error) in
                        
                        guard let success = success, error == nil else {
                            
                            if let networkError = error {
                                
                                switch(networkError) {
                                case .InvalidCredentials:
                                    
                                    if updateType == .checkOut {
                                        _ = SCLAlertView(appearance: appearance).showError("Not Registered", subTitle: "Please register first before check out").setDismissBlock {
                                            self.searchAttendeesTextField.becomeFirstResponder()
                                        }
                                    }
                                default:
                                    _ = SCLAlertView(appearance: appearance).showError("Network Error", subTitle: "\(networkError.rawValue)").setDismissBlock {
                                        self.searchAttendeesTextField.becomeFirstResponder()
                                    }
                                }
                                
                                self.searchAttendeesTextField.text = ""
                                
                                // Hide loading view
                                self.view.squareLoading.stop(0.0)
                            }
                            
                            return
                        }
                        
                        if success {
                            self.showConfetti()
                            _ = SCLAlertView(appearance: appearance).showSuccess("Success", subTitle: "Thank You \(item.title)").setDismissBlock {
                                self.confettiView.removeFromSuperview()
                                self.searchAttendeesTextField.becomeFirstResponder()
                            }
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "needToSync"), object: nil)
                        }else {
                            _ = SCLAlertView(appearance: appearance).showInfo("Registered", subTitle: "Hi \(item.title), you are already registered").setDismissBlock {
                                self.searchAttendeesTextField.becomeFirstResponder()
                            }
                        }
                        
                        self.searchAttendeesTextField.text = ""
                        self.view.squareLoading.stop(0.0)
                    })
                    
                })
                
                _ = alert.addButton("No", action: {
                    self.searchAttendeesTextField.text = ""
                    self.searchAttendeesTextField.becomeFirstResponder()
                })
                
                if updateType == .checkIn {
                    _ = alert.showWarning("Check In", subTitle: "Are you \(item.title)?")
                }else if updateType == .checkOut {
                    _ = alert.showWarning("Check Out", subTitle: "Are you \(item.title)?")
                }
            }
        }
    }
}
