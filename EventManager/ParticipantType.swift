//
//  ParticipantType.swift
//  EventManager
//
//  Created by Application Developer on 19/01/2017.
//  Copyright © 2017 Asian Hospital and Medical Center. All rights reserved.
//

import Foundation

enum ParticipantType: String {
    
    case all
    case active
    case inactive
}
