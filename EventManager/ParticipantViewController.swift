//
//  TestViewController.swift
//  EventManager
//
//  Created by Application Developer on 20/02/2017.
//  Copyright © 2017 Asian Hospital and Medical Center. All rights reserved.
//

import UIKit
import FoldingCell
import SCLAlertView
import AASquaresLoading

class ParticipantViewController : UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var ParticipantTableView: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    
    var validPincode: String!
    var titleString: String!

    var participants: [Attendees]!
    var filteredParticipants = [Attendees]()
    
    let kCloseCellHeight: CGFloat = 122
    let kOpenCellHeight: CGFloat = 475
    
    var cellHeights = [CGFloat]()
    
    let searchController = UISearchController(searchResultsController: nil)

    override func viewDidLoad() {
        super.viewDidLoad()

        createCellHeightsArray()
        configureSearchBar()
        configureAALoading()
        configureNotificationObserver()
        
        titleLabel.text = self.titleString
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.ParticipantTableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showAddParticipant" {
            if let manualRegistrationVC = segue.destination as? ManualRegistrationViewController {
                manualRegistrationVC.validPincode = validPincode
            }
        }
    }
    
    @IBAction func addButtonTapped(_ sender: UIButton) {
        self.performSegue(withIdentifier: "showAddParticipant", sender: self)
    }
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: FUNCTIONS
    
    func configureNotificationObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.getParticipants), name: NSNotification.Name(rawValue: "refreshParticipants"), object: nil)
    }
    
    func configureAALoading() {
        self.view.squareLoading.color = UIColor(red: 80/255.0, green: 187/255.0, blue: 113/255.0, alpha: 1.0)
        self.view.squareLoading.backgroundColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.7)
    }
    
    func getParticipants() {
        
        var participantType: ParticipantType!
        
        if self.titleString == "Participants" {
            participantType = .all
        }else{
            participantType = .active
        }
        
        self.view.squareLoading.start(0.0)
        
        getAttendees(passcode: validPincode, participantType: participantType) { (attendees, error) in
            
            guard let attendeesArray = attendees, error == nil else {
                
                if let networkError = error {
                    
                    // Hide loading view
                    self.view.squareLoading.stop(0.0)
                    _ = SCLAlertView(appearance: appearance).showError("Network Error", subTitle: "\(networkError.rawValue)")
                }
                
                return
            }
            
            self.participants = attendeesArray
            self.view.squareLoading.stop(0.0)
            
            if self.searchController.isActive && self.searchController.searchBar.text != "" {
                self.filterContentForSearchText(searchText: self.searchController.searchBar.text!)
            }else {
                self.ParticipantTableView.reloadData()
            }
        }
    }
    
    func createCellHeightsArray() {
        
        cellHeights.removeAll()
        
        if searchController.isActive && searchController.searchBar.text != "" {
            for _ in 0...filteredParticipants.count {
                cellHeights.append(kCloseCellHeight)
            }
        }else {
            for _ in 0...participants.count {
                cellHeights.append(kCloseCellHeight)
            }
        }
    }
    
    func filterContentForSearchText(searchText: String, scope: String = "All") {
        filteredParticipants = participants.filter { participants in
            return participants.displayName.lowercased().contains(searchText.lowercased()) || (participants.department.lowercased().contains(searchText.lowercased()))
            //                || (participants.employeeNumber?.contains(searchText))! ||
        }
        
        createCellHeightsArray()
        ParticipantTableView.reloadData()
    }
    
    func configureSearchBar() {
        
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        
        searchController.searchBar.textColor = UIColor.white
        searchController.searchBar.placeholder = "Search by name, department, and employee number"
        searchController.searchBar.searchBarStyle = .minimal
        searchController.searchBar.barTintColor = UIColor(red: 83/255.0, green: 91/255, blue: 108/255, alpha: 1.0)
        searchController.searchBar.tintColor = UIColor.white
        searchController.searchBar.backgroundColor = UIColor(red: 83/255.0, green: 91/255, blue: 108/255, alpha: 1.0)
        
        searchController.searchBar.isTranslucent = false
        
        self.ParticipantTableView.tableHeaderView = searchController.searchBar
    }
    
    // MARK: TABLE VIEW DATA SOURCE

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if searchController.isActive && searchController.searchBar.text != "" {
            return filteredParticipants.count
        }
        return participants.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FoldingCell", for: indexPath)
        
        return cell
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        guard case let cell as ParticipantCell = cell else {
            return
        }
        
        cell.backgroundColor = UIColor.clear
        
        if searchController.isActive && searchController.searchBar.text != "" {
            cell.participant = filteredParticipants[indexPath.row]
        }else {
            cell.participant = participants[indexPath.row]
        }

        if cellHeights[(indexPath as NSIndexPath).row] == kCloseCellHeight {
            cell.selectedAnimation(false, animated: false, completion:nil)
        } else {
            cell.selectedAnimation(true, animated: false, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeights[(indexPath as NSIndexPath).row]
    }
    
    // MARK: TABLE VIEW DELEGATE
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath) as! FoldingCell
        
        if cell.isAnimating() {
            return
        }
        
        var duration = 0.0
        
        if cellHeights[(indexPath as NSIndexPath).row] == kCloseCellHeight { // open cell
            cellHeights[(indexPath as NSIndexPath).row] = kOpenCellHeight
            cell.selectedAnimation(true, animated: true, completion: nil)
            duration = 0.3
        } else {// close cell
            cellHeights[(indexPath as NSIndexPath).row] = kCloseCellHeight
            cell.selectedAnimation(false, animated: true, completion: nil)
            duration = 0.5
        }
        
        UIView.animate(withDuration: duration, delay: 0, options: .curveEaseOut, animations: { () -> Void in
            tableView.beginUpdates()
            tableView.endUpdates()
        }, completion: nil)
        
    }
    
}

extension ParticipantViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchText: searchController.searchBar.text!)
    }
}

extension UISearchBar {
    
    var textColor: UIColor? {
        get {
            if let textField = self.value(forKey: "searchField") as? UITextField {
                return textField.textColor
            }else {
                return nil
            }
        }
        
        set (newValue) {
            if let textField = self.value(forKey: "searchField") as? UITextField {
                textField.textColor = newValue
                textField.font = UIFont(name: "HelveticaNeue", size: 20.0)
            }
        }
    }
}
