//
//  UpdateParticipantType.swift
//  EventManager
//
//  Created by Application Developer on 30/01/2017.
//  Copyright © 2017 Asian Hospital and Medical Center. All rights reserved.
//

import Foundation

enum UpdateParticipantType: String {
    
    case checkIn = "Check In"
    case checkOut = "Check Out"
}
