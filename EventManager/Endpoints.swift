//
//  Endpoints.swift
//  EventManager
//
//  Created by Steven on 06/01/2017.
//  Copyright © 2017 Asian Hospital and Medical Center. All rights reserved.
//

import Foundation

let BASE_URL = "https://hopprLab.com/API"

let PASSCODE_CHECKER_URL = "\(BASE_URL)/events/PasscodeChecker"
let PASSCODES_URL = "\(BASE_URL)/events/GetPasscodes"
let GET_PARTICIPANTS_URL = "\(BASE_URL)/events/GetParticipants"
let ADD_PARTICIPANT_URL = "\(BASE_URL)/events/AddParticipants"
let REGISTER_PARTICIPANT_URL = "\(BASE_URL)/events/UpdateRegisteredFlag"
let REGISTER_QR_PARTICIPANT_URL = "\(BASE_URL)/events/UpdateRegisteredFlagQR"
let BANNER_URL = "\(BASE_URL)/events/GetBanner/"
