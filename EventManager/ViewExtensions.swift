//
//  ViewExtensions.swift
//  EventManager
//
//  Created by HopprLab on 18/05/2017.
//  Copyright © 2017 Asian Hospital and Medical Center. All rights reserved.
//

import UIKit

extension UIView {
    
    func pushTransition(_ duration: CFTimeInterval) {
        let animation: CATransition = CATransition()
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        animation.type = kCATransitionPush
        animation.subtype = kCATransitionFromTop
        animation.duration = duration
        layer.add(animation, forKey: kCATransitionPush)
    }
    
}
