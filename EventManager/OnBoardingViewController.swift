//
//  OnBoardingViewController.swift
//  EventManager
//
//  Created by Application Developer on 07/02/2017.
//  Copyright © 2017 Asian Hospital and Medical Center. All rights reserved.
//

import UIKit
import paper_onboarding

class OnBoardingViewController: UIViewController, PaperOnboardingDataSource, PaperOnboardingDelegate {

    @IBOutlet weak var onboardingView: PaperOnboarding!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var getStartedButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        onboardingView.dataSource = self
        onboardingView.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // PaperOnBoarding Data Source
    
    func onboardingItemsCount() -> Int {
        return onBoardingItems.count
    }
    
    func onboardingItemAtIndex(_ index: Int) -> OnboardingItemInfo {

        let titleFont = UIFont(name: "AvenirNext-Bold", size: 30)!
        let descriptionFont = UIFont(name: "AvenirNext-Regular", size: 20)!
        
//        return (onBoardingItems[index].imageName, onBoardingItems[index].title, onBoardingItems[index].description, "", onBoardingItems[index].backgroundColor, UIColor.white, UIColor.white, titleFont, descriptionFont)
        
        return (UIImage(named: onBoardingItems[index].imageName)!,
        onBoardingItems[index].title,
        onBoardingItems[index].description,
        UIImage(),
        onBoardingItems[index].backgroundColor,
        UIColor.white,
        UIColor.white,
        titleFont,
        descriptionFont
        )
        
//        (imageName: UIImage, title: String, description: String, iconName: UIImage, color: UIColor, titleColor: UIColor, descriptionColor: UIColor, titleFont: UIFont, descriptionFont: UIFont)
    }

    // PaperOnBoarding Delegate
    
    func onboardingConfigurationItem(_ item: OnboardingContentViewItem, index: Int) {
        
    }
    
    func onboardingWillTransitonToIndex(_ index: Int) {
        if index == 1 || index == 0 {
            closeButton.isHidden = false
            getStartedButton.layer.removeAllAnimations()
            UIView.animate(withDuration: 0.2, animations: {
                self.getStartedButton.alpha = 0
            })
        }
    }
    
    func onboardingDidTransitonToIndex(_ index: Int) {
        if index == 2 {
            closeButton.isHidden = true
            buttonPulseAnimation(button: getStartedButton)
            UIView.animate(withDuration: 0.5, animations: {
                self.getStartedButton.alpha = 1
            })
        }else {
            UIView.animate(withDuration: 0.2, animations: {
                self.getStartedButton.alpha = 0
            })
        }
    }
}
