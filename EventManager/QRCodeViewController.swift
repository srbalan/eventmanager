//
//  QRCodeViewController.swift
//  EventManager
//
//  Created by HopprLab on 18/05/2017.
//  Copyright © 2017 Asian Hospital and Medical Center. All rights reserved.
//

import UIKit
import AVFoundation
import QRCodeReader
import SCLAlertView
import SAConfettiView

private let bannerImageName = "AHMC"

class QRCodeViewController: UIViewController {
    
    @IBOutlet weak var previewView: UIView!
    @IBOutlet weak var previewBackgroundView: UIView!
    @IBOutlet weak var notAuthorizedView: UIView!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var checkInOutLabel: UILabel!
    @IBOutlet weak var checkInOutSwitch: UISwitch!
    @IBOutlet weak var lockButton: UIButton!
    @IBOutlet weak var bannerImageView: UIImageView!
    @IBOutlet weak var backButton: UIButton!
    
    var validPincode: String!
    var confettiView: SAConfettiView!
    
    lazy var reader: QRCodeReader = QRCodeReader()
    
    lazy var readerVC: QRCodeReaderViewController = {
        let builder = QRCodeReaderViewControllerBuilder {
            $0.reader = QRCodeReader(metadataObjectTypes: [AVMetadataObjectTypeQRCode], captureDevicePosition: .back)
        }
        
        return QRCodeReaderViewController(builder: builder)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.checkInOutSwitch.addTarget(self, action: #selector(switchValueDidChange), for: .valueChanged)
        self.bannerImageView.image = UIImage(named: bannerImageName)
        self.checkInOutSwitch.isOn = false
        setupBackgroundView(view: previewBackgroundView)
        configureAALoading()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        startScanning()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        stopScanning()
    }

    // MARK: ACTIONS
    
    @IBAction func showSettingsButtonTapped(_ sender: UIButton) {
        showSettings()
    }
    
    @IBAction func switchCameraButtonTapped(_ sender: UIButton) {
        self.reader.switchDeviceInput()
    }
    
    @IBAction func lockButtonTapped(_ sender: UIButton) {
        let buttonImage = sender.currentImage
        
        if buttonImage == UIImage(named: "locked") {
            showUnlockView()
        }else if buttonImage == UIImage(named: "unlocked") {
            lockButton.setImage(UIImage(named: "locked"), for: .normal)
            backButton.isHidden = true
        }
    }
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: FUNCTIONS
    
    private func configureAALoading() {
        self.view.squareLoading.color = UIColor(red: 80/255.0, green: 187/255.0, blue: 113/255.0, alpha: 1.0)
        self.view.squareLoading.backgroundColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.7)
    }
    
    func switchValueDidChange() {
        var title = ""
        
        if checkInOutSwitch.isOn {
            title = "CHECK OUT"
        }else {
            title = "CHECK IN"
        }
        
        if let checkLabel = checkInOutLabel {
            checkLabel.pushTransition(0.4)
            checkLabel.text = title
        }
    }
    
    private func checkScanPermissions() -> Bool {
        do {
            return try QRCodeReader.supportsMetadataObjectTypes()
        } catch let error as NSError {
            switch error.code {
            case -11852:
                self.errorLabel.text = "This app is not authorized to use the camera"
            case -11814:
                self.errorLabel.text = "Reader not supported by the current device"
            default: break
            }
            
            self.notAuthorizedView.isHidden = false
            return false
        }
    }
    
    private func startScanning() {
        guard checkScanPermissions(), !reader.isRunning else { return }
        
        self.notAuthorizedView.isHidden = true
        
        reader.previewLayer.frame = previewView.bounds
        previewView.layer.addSublayer(reader.previewLayer)
        
        reader.startScanning()
        reader.didFindCode = { result in
            self.validateEmployeeNumber(employeeNumber: result.value)
        }
    }
    
    private func stopScanning() {
        guard reader.isRunning else { return }
        reader.stopScanning()
    }
    
    private func showSettings() {
        DispatchQueue.main.async {
            if let settingsURL = URL(string: UIApplicationOpenSettingsURLString) {
                UIApplication.shared.openURL(settingsURL)
            }
        }
    }
    
    private func setupBackgroundView(view: UIView) {
        view.layer.cornerRadius = 8.0
        view.layer.shadowOffset = CGSize(width: 0, height: 0)
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowRadius = 6
        view.layer.shadowOpacity = 0.35
        view.layer.masksToBounds = false
        view.clipsToBounds = false
    }
    
    private func showConfetti() {
        
        confettiView = SAConfettiView(frame: self.view.bounds)
        confettiView.intensity = 0.5
        confettiView.type = .Confetti
        view.addSubview(confettiView)
        
        confettiView.startConfetti()
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2.0, execute: {
            self.confettiView.stopConfetti()
        })
    }
    
    private func showUnlockView() {
        let alert = SCLAlertView(appearance: confirmationAppearance)
        let passcode = alert.addTextField("Passcode")
        passcode.isSecureTextEntry = true
        passcode.keyboardType = .decimalPad
        alert.addButton("Unlock") {
            if passcode.text == self.validPincode {
                self.lockButton.setImage(UIImage(named: "unlocked"), for: .normal)
                self.backButton.isHidden = false
                _ = SCLAlertView(appearance: appearance).showSuccess("Unlocked", subTitle: "")
            }else {
                _ = SCLAlertView(appearance: appearance).showError("Ooops!", subTitle: "Please enter a valid event passcode to unlock")
            }
        }
        alert.addButton("Cancel", action: { })
        alert.showNotice("Unlock", subTitle: "Please enter event passcode to unlock")
        
        // To focus in text field and show the keyboard
        passcode.becomeFirstResponder()
    }
    
    private func validateEmployeeNumber(employeeNumber: String) {
        
        var updateType: UpdateParticipantType!
        
        if self.checkInOutSwitch.isOn {
            updateType = .checkOut
        }else {
            updateType = .checkIn
        }
        
        self.view.squareLoading.start(0.0)
        
        updateParticipantQR(updateType: updateType, passcode: validPincode, employeeNumber: employeeNumber, completion: { (participant, error) in
            
            guard let participantDetails = participant, error == nil else {
                
                if let networkError = error {
                    
                    switch(networkError) {
                    case .AlreadyRegistered:
                        _ = SCLAlertView(appearance: appearance).showInfo("Registered", subTitle: "\(networkError.rawValue)").setDismissBlock {
                            self.reader.startScanning()
                        }
                    case .NotYetRegistered:
                        _ = SCLAlertView(appearance: appearance).showError("Error", subTitle: "\(networkError.rawValue)").setDismissBlock {
                            self.reader.startScanning()
                        }
                    case .InvalidCredentials:
                        _ = SCLAlertView(appearance: appearance).showError("Invalid", subTitle: "QR Code is invalid. Please try again").setDismissBlock {
                            self.reader.startScanning()
                        }
                    default:
                        _ = SCLAlertView(appearance: appearance).showError("Network Error", subTitle: "\(networkError.rawValue)").setDismissBlock {
                            self.reader.startScanning()
                        }
                    }
                }
                
                // Hide loading view
                self.view.squareLoading.stop(0.0)
                
                return
            }
            
            self.showConfetti()
            _ = SCLAlertView(appearance: appearance).showSuccess("Success", subTitle: "Thank You \(participantDetails.lastName), \(participantDetails.firstName)").setDismissBlock {
                self.confettiView.removeFromSuperview()
                self.reader.startScanning()
            }
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "needToSync"), object: nil)
            
            // Hide loading view
            self.view.squareLoading.stop(0.0)
        })
    }
    
}
